import pandas as pd
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split
import numpy as np
from math import sqrt
import copy 

'''
Calcula a distancia entre 2 pontos

'''
def distance(element1,element2):
	dist = 0

	for i in range(len(element1)):
		dist += (element1[i]-element2[i])**2

	return sqrt(dist)



'''
Retorna a classe da instância que está em x_train que é a mais próximo de element

'''
def most_close_class(element,x_train,y_train):


	min_distance = float('inf')
	most_close = 0

	for i in range(np.shape(x_train)[0]):
		
		current_distance = distance(element,x_train[i])
		if current_distance < min_distance:
			
			min_distance = current_distance
			most_close = y_train[i]


	return most_close


'''
Retorna o item que mais se repete em uma lista
'''
def most_common(lst):
    return max(set(lst), key=lst.count)

def main():
	

	data = pd.read_csv('wine.data',names=['class','atr1','atr2','atr3','atr4','atr5','atr6','atr7','atr8','atr9','atr10','atr11','atr12','atr13'])

	x_train, x_test = train_test_split(data, test_size=0.2, random_state=42, shuffle=True)
		


	'''
	Separa e exclui os labels do dataset
	'''
	y_train = list(x_train['class'])
	del x_train['class']

	y_test = list(x_test['class'])
	del x_test['class']

	x_train = normalize(x_train, axis=0)
	x_test = normalize(x_test, axis=0)

	k = 10
	classes = [0]*k
	acc = 0

	for j in range(np.shape(x_test)[0]):
		
		for i in range(k):

			most_close = most_close_class(x_test[j],x_train,y_train)
			classes[i] = most_close

		if most_common(classes) == y_test[j]:
			acc+=1


	print('Acc:',acc,'Total:',np.shape(x_test)[0],"Rate:",acc/np.shape(x_test)[0])
	
if __name__ == '__main__':
	main()